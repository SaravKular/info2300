/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var app = {
    // Application Constructor
    initialize: function() {
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
    },

    // deviceready Event Handler
    //
    // Bind any cordova events here. Common events are:
    // 'pause', 'resume', etc.
    onDeviceReady: function() {
        this.receivedEvent('deviceready');
        
        if (!$('#ulProducts').hasClass('ui-listview')) $('#ulProducts').listview();
         if (!$('#invoiceList').hasClass('ui-listview')) $('#invoiceList').listview();
         var totalAmount=0;
         var code = $("#inputCodeNew").val();
         var price = $("#inputPriceNew").val();
         var product = $("#inputProductNew").val();
         var price_regex = /^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/;
         //^(?!(?:0|0\.0|0\.00)$)[+]?\d+(\.\d|\.\d[0-9])?$/;

         $("#buttonSave").click(function (event) {
            code = $("#inputCodeNew").val();
            price = $("#inputPriceNew").val();
            product = $("#inputProductNew").val();

            if (code.length == 0) {
               $("#saveError").text("Please enter a code");
               $("#inputCodeNew").focus();
               $("#inputCodeNew").css("border-color", " red");
               return false;
            }
            else if (!price.match(price_regex) || price.length == 0) {
               $("#saveError").text("Plese enter a valid price");
               $("#inputPriceNew").focus();
               return false;
            }
            else if (!(product.length <= 20 && product.length >= 2) || product.length == 0) {
               $("#saveError").text("Plese enter a name between 2 to 20 characters");
               $("#inputProductNew").focus();
               return false;
            }
            else if(storeManager.checkItemCode(code)){
               $("#saveError").text("Code already in use");
            }
            else{
               storeManager.addItem(code,price,product);
               storeManager.refreshList();
               $("#saveError").text("Product Saved");
               $("#inputCodeNew").val("");
               $("#inputPriceNew").val("");
               $("#inputProductNew").val("");
            }
         })
         $("#cbClearOnStartup").select(function(event){
            $("#inputCodeNew").empty();
            $("#inputProductNew").empty();
            $("#inputPriceNew").empty();
         })
         $("#buttonClearDb").click(function(event){
            storeManager.clearDb();
         })

         $("#inventoryForm").submit(function (event) {

            event.preventDefault();
            return false;

         })
         $("#buttonAdd").click(function (event) {
            var inputProduct= $("#inputProduct").val();
            if ($("#inputProduct").val() == "") {
               $("#scanError").text("No Code Entered");
               $("#inputProduct").focus();
            }
            else if (storeManager.checkItemCode(inputProduct) == false) {
               $("#scanError").text("Invalid Code Entered");
               $("#inputProduct").focus();
            }
            else if (storeManager.checkItemCode(inputProduct)==true) {
                  var jqLi =$('<li>');
                     jqLi.text(storeManager.storeItem[inputProduct].product);
                  var jqSpan = $('<span>').text(storeManager.storeItem[inputProduct].price).addClass('ui-li-count');
                  jqLi.append(jqSpan);
                  $('#invoiceList').append( jqLi );
                  $('#invoiceList').listview('refresh');
                  totalAmount=totalAmount+parseInt(price);
                  $("#totalAmount").text("$"+totalAmount);
                  //storeManager.storeItem[inputProduct].sellOne();
            }
            $("#scannerPage").submit(function (event) {
               event.preventDefault();
               return false;
            })

            $("#buttonReset").click(function (event) {
                  $('#invoiceList').empty();
                  storeManager.clearDb();
                  $("#invoiceList").listview('refresh');
            })
            event.preventDefault();
            return false;
         })
         $("#buttonScan").click(function (event) {
            var inputProduct= $("#inputProduct").val();
            if ($("#inputProduct").val() == "") {
               $("#scanError").text("No Code Entered");
               $("#inputProduct").focus();
            }
            else if (storeManager.checkItemCode(inputProduct) == false) {
               $("#scanError").text("Invalid Code Entered");
               $("#inputProduct").focus();
            }
            else if (storeManager.checkItemCode(inputProduct)==true) {
                  var jqLi =$('<li>');
                     jqLi.text(storeManager.storeItem[inputProduct].product);
                  var jqSpan = $('<span>').text(storeManager.storeItem[inputProduct].price).addClass('ui-li-count');
                  jqLi.append(jqSpan);
                  $('#invoiceList').append( jqLi );
                  $('#invoiceList').listview('refresh');
                  totalAmount=totalAmount+parseInt(price);
                  $("#totalAmount").text("$"+totalAmount);
                  //storeManager.storeItem[inputProduct].sellOne();
            }
            $("#scannerPage").submit(function (event) {
               event.preventDefault();
               return false;
            })

            $("#buttonReset").click(function (event) {
                  $('#invoiceList').empty();
                  storeManager.clearDb();
                  $("#invoiceList").listview('refresh');
            })
            event.preventDefault();
            return false;
         })
         $(function () {
            $("[data-role='header'], [data-role='footer']").toolbar();
         });
         $(function () {
            $("[data-role='navbar']").navbar();
            $("[data-role='header'], [data-role='footer']").toolbar();
         });
         $(function(){
            $( "[data-role='header']" ).toolbar({ theme: "a" });
         });
         // Update the contents of the toolbars
         $(document).on("pagecontainerchange", function () {
            // Each of the four pages in this demo has a data-title attribute
            // which value is equal to the text of the nav button
            // For example, on first page: <div data-role="page" data-title="Info">
            var current = $(".ui-page-active").jqmData("title");
            // Change the heading
            $("[data-role='header'] h1").text(current);
            // Remove active class from nav buttons
            $("[data-role='navbar'] a.ui-btn-active").removeClass("ui-btn-active");
            // Add active class to current nav button
            $("[data-role='navbar'] a").each(function () {
               if ($(this).text() === current) {
                  $(this).addClass("ui-btn-active");
               }
            });
         });
         storeManager.initialize('Database');
         if(storeManager.getClearOnStartup()==true){
            storeManager.clearDb();
            $("#cbClearOnStartup").prop('checked',true);
         }
         else{
            $("#cbClearOnStartup").prop('checked',false);
         }
         $("#cbClearOnStartup").change(function(){
            storeManager.setClearOnStartup("ClearOnStartup")
        })
    },

    // Update DOM on a Received Event
    receivedEvent: function(id) {
        var parentElement = document.getElementById(id);
        var listeningElement = parentElement.querySelector('.listening');
        var receivedElement = parentElement.querySelector('.received');

        listeningElement.setAttribute('style', 'display:none;');
        receivedElement.setAttribute('style', 'display:block;');

        console.log('Received Event: ' + id);
    }
    
};

app.initialize();

document.addEventListener("deviceready", onDeviceReady, false);
function onDeviceReady() {
    console.log(navigator.vibrate);
}

setTimeout(function() {
   navigator.splashscreen.hide();
}, 2000);

if(document.getElementById('buttonScan')){
   document.getElementById('buttonScan').onclick=function(){
cordova.plugins.barcodeScanner.scan(
   function (result) {
       alert("We got a barcode\n" +
             "Result: " + result.text + "\n" +
             "Format: " + result.format + "\n" +
             "Cancelled: " + result.cancelled);
   },
   function (error) {
       alert("Scanning failed: " + error);
   }
   )
};

TTS
    .speak({
        text: '$'+'#totalAmount',
        locale: 'en-GB',
        rate: 0.75
    }, function () {
        alert('success');
    }, function (reason) {
        alert(reason);
    });