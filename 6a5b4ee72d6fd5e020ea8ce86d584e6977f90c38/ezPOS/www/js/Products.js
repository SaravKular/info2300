var storeManager = {
    db: null,
    storeItem: {},
    onDbError: function() {
      console.error('DB error: ', arguments);
    },
    initialize: function(dbName) {
        var self = this;
    
        this.db = openDatabase(dbName, '1.0', 'ezPOS Products', 1024 * 1024,
          function() { console.log('DB created'); });
        
        this.db.transaction(
          function callback(tx) {
            tx.executeSql('CREATE TABLE IF NOT EXISTS ' +
              'storeItem(code VARCHAR(13), price FLOAT, ' +
              'product VARCHAR(64),numberSold INTEGER,totalSales INTEGER)', 
              [], 
                 
              function() {
                
                tx.executeSql('SELECT * FROM storeItem', 
                  [], 
                  function(_tx, resultSet) {
                    for (var i = 0; i < resultSet.rows.length; i++) {
                      self.storeItem[resultSet.rows[i].code] = 
                        new StoreItem(
                          resultSet.rows[i].code, 
                          resultSet.rows[i].price, 
                          resultSet.rows[i].product,
                          resultSet.rows[i].numberSold,
                          resultSet.rows[i].totalSales
                        );
                    }
                  }
                );
                storeManager.refreshList();
              },
              function onError() {
                // Alert the user to a database error if desired
                console.error('Transaction error');
              }
            );
          }, 
          function onError() {
            // Alert the user to a database error if desired
            console.error('Transaction error');
          }
        );
      },
    checkItemCode: function(code){
      for(var item in storeManager.storeItem){
        if(item == code){
            return true;
        }
      } 
      return false;
    },
    addItem : function(code, price, product) {
        if (!this.storeItem.hasOwnProperty(code)) {
          this.storeItem[code] = new StoreItem(code, price, product);
    
          this.db.transaction(
            function callback(tx) {
              tx.executeSql(
                'INSERT INTO storeItem(code, price, product) VALUES (?, ?, ?)', 
                [code, price, product], 
                undefined, 
                function onError() {
                  // Alert the user to a database error if desired
                  console.error('SQL insert statement error', arguments);
                }
              );
            }, 
            function onError() {
              // Alert the user to a database error if desired
              console.error('Transaction error');
            }
          );
        }
        else 
          console.error('Item already exists');
      },
    removeItem :function(code) {
        delete(this.storeItem[code]);
        
        this.db.transaction(
          function callback(tx) {
            tx.executeSql(
              'DELETE FROM storeItem WHERE code = ?', 
              [code], 
              undefined, 
              function onError() {
                // Alert the user to a database error if desired
                console.error('SQL delete statement error', arguments);
              }
            );
            storeManager.refreshList();
          }, 
          function onError() {
            // Alert the user to a database error if desired
            console.error('Transaction error');
          }
        );
      },
      
      refreshList :function(){
      $("#ulProducts").empty();
        for(var code in this.storeItem){
          // this.storeItem[code].generateListItem();
          var deleteListItem=$('<a>');
         deleteListItem.click(function(){
           storeManager.removeItem(code);
         })
          var productListItem=$('<li><a>' + this.storeItem[code].product +'<br></br>'+'Code:'+this.storeItem[code].code+', Price:'+ this.storeItem[code].price +'<br></br>'+'Sold:'+this.storeItem[code].numberSold+', Sales:'+ this.storeItem[code].totalSales + '</a></li>');
          productListItem.append(deleteListItem);
          $("#ulProducts").append(productListItem);

      $("#ulProducts").listview("refresh");
      }},
    
     clearDb :function() {
  
      this.db.transaction(
        function callback(tx) {
          tx.executeSql(
            'DELETE FROM storeItem', 
            [], 
            undefined, 
            function onError() {
              // Alert the user to a database error if desired
              console.error('SQL delete all statement error', arguments);
            }
          );
          storeManager.storeItem = {};
          storeManager.refreshList();
        }, 
        function onError() {
          // Alert the user to a database error if desired
          console.error('Transaction error');
        }
      );
    },

    setClearOnStartup:function(clearOnStartup){
      if($("#cbClearOnStartup").prop('checked')==true)
        {
          var result=true;
        }
        else{
          var result=false;
        }
      localStorage.setItem(clearOnStartup,result)
    },
     getClearOnStartup:function(){
      return JSON.parse(localStorage.getItem("ClearOnStartup"));
     }
  };
  
    function StoreItem (code, price, product, numberSold, totalSales) {
        this.code = code;
        this.price = price;
        this.product=product;
        if(numberSold==null){
          this.numberSold=0;
        }
        else{
          this.numberSold=numberSold;
        }
        if(totalSales==null){
          this.totalSales=0;
        }
        else{
        this.totalSales=totalSales;
        }
      }
  
  
  StoreItem.prototype.generateListItem = function () {
         var code = this.code;
         var price = this.price;
         var product =this.product;

         var deleteListItem=$('<a>');
         deleteListItem.click(function(){
           storeManager.removeItem(code);
           
         })

         var productListItem=$('<li><a>' + product +'<br></br>'+'Code:'+code+', Price:'+ price +'<br></br>'+'Sold:'+this.numberSold+', Sales:'+ this.totalSales + '</a></li>');
        
         productListItem.append(deleteListItem);
         $("#ulProducts").append(productListItem);

         
      };

      StoreItem.prototype.sellOne=function(){
        this.numberSold=this.numberSold+1;
        this.totalSales=this.totalSales+this.price;
        var self=this;
        storeManager.db.transaction(
          function callback(tx){
            tx.executeSql('UPDATE storeItem SET numberSold =?,totalSales=? WHERE code=?',
            [self.numberSold, self.totalSales, self.code], undefined, function(){storeManager.onDbError}())
          },
        )
      }
    
        