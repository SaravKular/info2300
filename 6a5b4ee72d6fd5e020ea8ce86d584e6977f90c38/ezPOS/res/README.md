### Add the following to config.xml

    <splash density="land-hdpi" src="res/screen/android/screen-hdpi-landscape.png" />
    <splash density="land-ldpi" src="res/screen/android/screen-ldpi-landscape.png" />
    <splash density="land-mdpi" src="res/screen/android/screen-mdpi-landscape.png" />
    <splash density="land-xhdpi" src="res/screen/android/screen-xhdpi-landscape.png" />
    <splash density="port-hdpi" src="res/screen/android/screen-hdpi-portrait.png" />
    <splash density="port-ldpi" src="res/screen/android/screen-ldpi-portrait.png" />
    <splash density="port-mdpi" src="res/screen/android/screen-mdpi-portrait.png" />
    <splash density="port-xhdpi" src="res/screen/android/screen-xhdpi-portrait.png" />
    <icon density="ldpi" src="res/icon/android/icon-36-ldpi.png" />
    <icon density="mdpi" src="res/icon/android/icon-48-mdpi.png" />
    <icon density="hdpi" src="res/icon/android/icon-72-hdpi.png" />
    <icon density="xhdpi" src="res/icon/android/icon-96-xhdpi.png" />