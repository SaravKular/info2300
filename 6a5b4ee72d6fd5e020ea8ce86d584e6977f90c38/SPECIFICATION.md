# Mobile POS: Accessing device hardware and finalizing the app

Use the Cordova bridge library to gain access to device hardware, and also use Cordova Command Line Interface (CLI) Tools to package the app for Android devices. Write a unit test to help catch bugs with any future iterations of the code.

## General

The app must be viewable and usable in the browser debugger when set to simulate a mobile device. At the very least it should be viewable and usable on a Pixel 2 and in both orientations (landscape and portrait).

The app must also be viewable and usable in Android Studio's emulator running Pixel 2 when installed from an APK.

## Install Cordova

In the `assignment3` folder, setup Cordova and add `browser` as a platform. Follow the "Get Started Fast" guide at https://cordova.apache.org/.

The name of the app should be `ezPOS`.

Copy your source code (HTML, JS + JQuery + JQueryMobile, CSS) and custom logo from `assignment2` into the appropriate files and folders in `assignment3/ezPOS/www`.

Make sure in your `index.html` the Cordova JavaScript library is imported before your `js/index.js` file.

When merging `index.css`, you are free to make the application look the way you want. If you want to remove the `body` and `h1` CSS rules supplied by Cordova, for example, you can make your app return to its original look from Assignments 1 and 2.

Inside of `assignment3/ezPOS/config.xml`, change the Java package name (`widget`'s `id` attribute) to `ca.on.conestogac.ezPOS`. Change the `<name>` to **ezPOS**, and modify `<author>` and `<description>`.

Add `android` as a platform for Cordova:

```
cordova platform add android
```

## Add Cordova plugins

There are many plugins for scanning barcodes, QR codes and other codes for Cordova. In this assignment, we will use one particular plugin that provides access to the device's camera, and reads a Universal Product Code (UPC) in real life.

In the `assignment3/ezPOS` folder:

```
cordova plugin add cordova-plugin-barcodescanner
```

The app will also vibrate the device to indicate when a valid code was scanned.

In the `assignment3/ezPOS` folder:

```
cordova plugin add cordova-plugin-vibration
```

When a valid code is scanned, the app will read out the item's price.

In the `assignment3/ezPOS` folder:

```
cordova plugin add cordova-plugin-tts
```

The app will show a splash screen on boot.

In the `assignment3/ezPOS` folder:

```
cordova plugin add cordova-plugin-splashscreen
```

There are many plugins to perform these tasks with native capabilities, but use the plugins specified above for this assignment.

## Change your UI

Ensure your entire app is capable of handling item codes up to **13** characters in length.

Increase the `maxlength` property to **13** for the input with id `inputCodeNew` inside of your **Products** page and the input with id `<-- CODEID -->` inside of your **Scanner** page.

Make sure in your `Products.js` the database table can handle item codes up to **13** characters in length.

Add a new button to the right of your button with id `buttonAdd` with id `buttonScan`. Make the `buttonScan` have the same jQueryMobile attributes as `buttonAdd`, and add the following attribute to both:

```
data-iconpos="notext"
```

If you are using `<a>` instead of `<button>`, add the following class for a no-text button:

```
ui-btn-icon-notext
```

The layout of the two buttons should look similar to the screenshot below (i.e. the two buttons in the same row as the input and total, no text showing):

![spec1](res/spec1.png)

## Integrate plugins with your code

Take a look at the documentation for the four plugins: https://www.npmjs.com/package/cordova-plugin-barcodescanner, https://www.npmjs.com/package/cordova-plugin-vibration, https://www.npmjs.com/package/cordova-plugin-tts and https://www.npmjs.com/package/cordova-plugin-splashscreen.

When the button with id `buttonScan` is pressed, the barcode plugin's scan function should be called so that the app user can scan a physical UPC-13 barcode (see example image at end of this specification).

The value of the barcode returned by the plugin should be checked against any saved item code from the **Products** page. 

The behaviour of entering the code manually via the input box `inputCode` and  pressing the button `buttonAdd` should be the same as scanning a code via the plugin after pressing the button `buttonScan`: if the code matches an item, then the item is added to the invoice and the total is updated. 

Whether entering the code manually or using the scanner plugin, call the appropriate method from the vibration plugin to vibrate the device for 500ms every time a valid barcode is scanned. Also for every valid code scanned, the price of the item scanned should be emitted out loud using the text-to-speech plugin.

If the code does not match an item, the UI should update with the same error message in red, "Product not found" (same behaviour from `assignment1` if a bad code is entered).

Hint: if the barcode plugin takes too long to close and the device doesn't vibrate or emit sound on valid codes, you are free to use a workaround such as [`setTimeout()`](https://developer.mozilla.org/en-US/docs/Web/API/WindowOrWorkerGlobalScope/setTimeout) to delay the sound and vibration by an appropriate amount of time.

## Finishing touches

Update the splash screen images `assignment3/<-- TITLE -->/res/splash/android` so that a custom splash screen will show on app startup.

Update the icon images inside of `assignment3/<-- TITLE -->/res/icon/android` with your own custom icon.

Edit `config.xml` so that Cordova will use the `res/splash/android` and `res/icon/android` images and override the default splash screens and icons originally inside `/platforms/android/res`. To do so, inside of the `<platform name="android">` tag, insert:

      <splash density="land-hdpi" src="res/screen/android/screen-hdpi-landscape.png" />
      <splash density="land-ldpi" src="res/screen/android/screen-ldpi-landscape.png" />
      <splash density="land-mdpi" src="res/screen/android/screen-mdpi-landscape.png" />
      <splash density="land-xhdpi" src="res/screen/android/screen-xhdpi-landscape.png" />
      <splash density="port-hdpi" src="res/screen/android/screen-hdpi-portrait.png" />
      <splash density="port-ldpi" src="res/screen/android/screen-ldpi-portrait.png" />
      <splash density="port-mdpi" src="res/screen/android/screen-mdpi-portrait.png" />
      <splash density="port-xhdpi" src="res/screen/android/screen-xhdpi-portrait.png" />
      <icon density="ldpi" src="res/icon/android/icon-36-ldpi.png" />
      <icon density="mdpi" src="res/icon/android/icon-48-mdpi.png" />
      <icon density="hdpi" src="res/icon/android/icon-72-hdpi.png" />
      <icon density="xhdpi" src="res/icon/android/icon-96-xhdpi.png" />

When editing any of the `res` PNGs, you may find it easier to design the largest size splash screen or icon first, then resize that image down to the other resolutions to take care of the other image sizes.

You can also look for online resources such as an icon generator to help you with these PNGs. One such example is [Android Asset Studio](https://romannurik.github.io/AndroidAssetStudio/).

It is a good idea to correct any lingering bugs from Assignments 1 and 2 so that they do not replicate in your finished product.

## Write a unit test

Create a new file `qunit.html` inside of `assignment3/ezPOS/`.

The contents of `qunit.html` should resemble the sample provided in the documentation: https://qunitjs.com/intro/#the-qunit-javascript-test-suite

Create at least one unit test using QUnit that tests one or more of the functions you implemented inside the global object in `Products.js`. That is, in the documentation sample, you should replace `prettydate.js` with the correct path to your `Products.js`, `prettydate basics` with something more informative depending on what you are testing, and the testing code inside to something more meaningful. Provide at least one assertion in the test code.

Open `qunit.html` to verify that your test runs and gives the correct result. Test that when you modify the code inside `Products.js` to be appropriately buggy based on the test you wrote, the unit test correctly reports a failure in the assertion.

## Test your app

Test your app thoroughly in Android Studio Emulator or via the Android Debug Bridge (ADB) if you decide to connect your own Android device to your computer via USB.

If you test in an Emulator setting, you will need access to a webcam (if your laptop has a built-in webcam, that is fine to use).

Here is one sample barcode you can use (item code: `1234567890128`):

![spec2](res/spec2.png)

Test with other barcodes from random items in your residence or using barcode generators online.

Make sure your code can compile into an APK from source and run on an Emulator (Pixel 2 API 24) or your own Android Device.

You will be required to perform a brief two to three-minute demo. You can choose to demo on your own machine/device or the machine on the lectern. If you decide to use the lectern machine, your code will be pulled from repository (which is a replica of the code submitted to eConestoga). If you decide to use your own machine, you will be asked to follow a few steps that will prove the code you are running is the code submitted to eConestoga.

In the demo, you will need to demonstrate without assistance:

* The code compiles to an APK via `cordova platform add android` and `cordova build`
* The APK runs on an Android device via Android Debug Bridge, either to a USB connected Android device or to a virtual device on Android Studio emulator
* The app installs without error, and your app shows in the OS app menu with your custom icon and proper title
* The app runs without error, shows a custom splash screen that hides automatically
* Two test items (codes, prices and names are given to you at demo time) are processed correctly by your **Products** page and the items are saved to the database
* The two test items' barcodes are correctly scanned
* On each successful scan, the app vibrates and announces the prices (if on emulator, the vibrate action should show in Android Studio's logs in the IDE)
* The app displays the correct invoice and total
* A third test item is scanned, but the app correctly reports it as an unknown item. The device should not vibrate or announce any price for unknown items.


<!-- eyJUSVRMRSI6ImV6UE9TIiwiVEhFTUUiOiJiIiwiU0NBTk5FUiI6IlNjYW5uZXIiLCJJTlZFTlRPUlkiOiJQcm9kdWN0cyIsIkFCT1VUIjoiQWJvdXQiLCJJTlZFTlRPUllJQ09OIjoic2hvcCIsIkFCT1VUSUNPTiI6Im1haWwiLCJDT0RFTUFYIjoiNSIsIlBST0RVQ1RNQVgiOiIyMCIsIk5PQ09ERSI6IlBsZWFzZSBlbnRlciBhIGNvZGUiLCJUT1RBTElEIjoidG90YWxBbW91bnQiLCJDT0RFSUQiOiJpbnB1dENvZGUiLCJBRERCVE5JRCI6ImJ1dHRvbkFkZCIsIkNMRUFSQlROSUQiOiJidXR0b25SZXNldCIsIlBST0RVQ1ROT1RGT1VORCI6IlByb2R1Y3Qgbm90IGZvdW5kIiwiQ09ERUlOVVNFIjoiQ29kZSBhbHJlYWR5IGluIHVzZSIsIlNDQU5CVE5JRCI6ImJ1dHRvblNjYW4ifQ== -->