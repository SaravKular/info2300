# Prog 2430 Assignment 3

**Assigned**: Thursday, October 10, 2019

**Due**: Thursday, October 31, 2019 before start of class

**Preparation**: 
  * All course material from Weeks 1 to 7
  * Assignment 2 completed and corrected so that it has no significant bugs or programming style issues

**Demonstration**: 
  * In-class (privately), Thursday, October 31, 2019

---

## Instructions:

Your client wishes to have a simple POS (point-of-sale) app on their mobile phone, with the ability to scan QR codes with their phone camera, keeping track of inventory and sales.

Over the course of three assignments, you will design this POS app using jQuery, jQueryMobile, local storage techniques, and native features via Cordova.

This particular assignment focuses on integrating Cordova plugins to interface with device hardware. The development cycle for this app ends with packaging into an Android Package (APK) and deploying unit tests.

Please note there is a demonstration component to this Assignment. On-time submissions will be demonstrated on Thursday, October 31, 2019 during class (privately, not in front of the class). Late submissions or absentees will be required to demonstrate during a break or immediately before or after any class thereafter (but no later than 2 weeks after the due date).

***Getting started***:

Make sure Assignment 2 is complete, and any necessary corrections are made to it so that it will run without any significant bugs. Resolve any programming standard infractions in its source files.

To get started on Assignment 3, change directory to `assignment3` from your `Prog2430-Assignments` directory and run `npm install`:

    cd assignment3
    npm install

When `npm install` completes (with no errors), in your `assignment3` folder you will see a file `SPECIFICATION.md`.

***Committing and pushing your changes to the remote repository***:

Save the changes in your `assignment3` folder by making a commit to your repository and pushing the changes to the remote server:

    git add --all
    git commit -am "short description of your changes"
    git push origin master

`git add --all` says to make sure to track any new files added or old files deleted.

`git commit -am` commits your changes locally. A good description to add here might be something like `git commit -am "Add spec for assignment 3"`.

`git push origin master` copies your changes with the descriptive commit to the remote repository. Your work is now successfully backed up to the remote repository.

Do the above three `git` commands in sequence (replacing "short description of your changes" with your own description) every time you finish a feature, fix a bug, or do any change that logically merits a commit with a short description. You should test to see that your feature or bug fix works prior to committing/pushing.

Part of the evaluation includes making regular commits with good descriptions. It is **highly** recommended to work on assignments as you progress through the course material. Do not leave this assignment to the last minute as you will inevitably make commits that lump unrelated changes together.

Once you've pushed your first commit to the remote server, view `SPECIFICATION.md`. You can view `SPECIFICATION.md` by going to the [remote repository](https://git.acsit.cc) and clicking on `assignment3` and then `SPECIFICATION.md` in your Prog2430-Assignments repository, or within Visual Studio Code. 

If you open it in Visual Studio Code, CTRL+K and then V will open up a markdown viewer window that will render the file with pictures and formatting.

Pretend this is an actual specification given to you by a client or employer and you are delivering the product.

Do the work, remembering to commit and push your changes along the way. Do not rename, modify or delete the `SPECIFICATION.md` file.

***When you are done the assignment***:

When you are done implementing everything in the specification, make sure you commit and push any remaining changes on your computer to the remote repository.

Then, inside the `assignment3` folder, run the following:

    npx done a3

You will see a new file ending in `.a3.zip` in your Prog2430-Assignments folder (the parent folder of `assignment3`). Do not rename this file. Upload this file to eConestoga > [Assignments](https://conestoga.desire2learn.com/d2l/lms/dropbox/user/folders_list.d2l?ou=285784&isprv=0).

Once you've uploaded the `.zip` file to eConestoga, do not make any more commits or push to the remote repository. This ensures the contents of the `.zip` file matches the contents in the remote repository at the time of evaluation.

<div class="page"/>

## Evaluation: 

Everything with "(Demo)" in the notes will be evaluated during the demonstration. All other items will be evaluated outside of the demonstration.

| Item | Mark | Notes |
| --- | --- | --- |
| **Visual and UI Updates** | /***19***
| Cordova configuration | 7 | config.xml correctly edited for package name, title, description, author, Android icon and splash screen resources
| Correct app title | 2 | (Demo)
| Custom app icon | 2 | (Demo) Must not be default Apache Cordova icon
| Scan button | 2 | (Demo) Must be no-text
| Add button | 2 | (Demo) Must be no-text
| Buttons on same row | 2 | (Demo) Buttons must be inline
| Correct attributes on inputs and buttons | 2 
| **Splash Screen** | /***10***
| Plugin correctly installed | 2
| Custom splash screen | 4 | (Demo) Must not be default Apache Cordova splash screen
| Splash screen shows on startup | 2 | (Demo)
| Splash screen hides eventually | 2 | (Demo) 
| **Vibration** | /***5***
| Plugin correctly installed | 2 |
| Vibrate on first valid scan | 1 | (Demo)
| Vibrate on second valid scan | 1 | (Demo)
| No vibrate on invalid scan | 1 | (Demo)
| **Text to Speech** | /***8***
| Plugin correctly installed | 2 |
| Correct price spoken on first valid scan | 2 | (Demo)
| Correct price spoken on second valid scan | 2 | (Demo)
| No sound on invalid scan | 2 | (Demo) 
| **Barcode Scanner** | /***28***
| Plugin correctly installed | 4 |
| Addition of two items to inventory | 6 | (Demo)
| Camera activated on button press | 2 | (Demo)
| Barcode plugin error handled gracefully | 4 |
| Valid scan of first item, item added to invoice | 2 | (Demo)
| Valid scan of first item, total updated correctly | 2 | (Demo)
| Valid scan of second item, item added to invoice | 2 | (Demo)
| Valid scan of second item, total updated correctly | 2 | (Demo)
| Scan of bogus item, item not added to invoice | 2 | (Demo)
| Scan of bogus item, total remains the same | 2 | (Demo)
| **Practical Knowledge** | /***10***
| Build the APK | 5 | (Demo) Must not require assistance
| Install and run APK on Device or Emulator | 5 | (Demo) Must not require assistance
| **Unit Test** | /***16***
| QUnit HTML file | 8 | Appropriate title, correct path to source file, correct path to QUnit library and correct layout when opened in browser
| At least one meaningful test written | 8 | Clearly tests an important functionality of Assignment 2 API and accurately reports success/failure; clear how the test would fail upon faulty modification of source
| **Repository** | /***4***
| - Regular, Meaningful Commits | 4 | 1 mark per commit
| **Total** | **/100**

**Deductions**:

| Item | Per Infraction | To a Maximum | Notes |
| --- | --- | --- | --- |
| Assignment Standards | 5 | 20 | See slide deck #0 from Week 1. Renaming the `.a3.zip` file, pushing new changes after the `.a3.zip` file was submitted, adding any additional files outside of the files Cordova adds, or modifying or adding any file besides the `index.*`, `qunit.html`, or the inventory source file are considered assignment standard infractions. **Not demonstrating the final product within two weeks of the due date will result in the maximum Assignment Standards deduction of 20**.
| Programming Standards | 1 | 20 | See list of programming standards enforced below.
| Lateness | | 100 | See slide deck #0 from Week 1.

**Programming Standards**:

List of infractions observed for this assignment:

* Unnecessary messages, warnings or errors appearing in debugger console

* JavaScript programming standards
  * identifiers not in camelcase (P3)
  * magic numbers (P9)
  * lines exceeding 160 characters (P12)
  * confusing multiline expressions (P13)
  * unreachable code (P21)
  * assignment operators in conditional statements (P23)
  * constant expressions in conditions (P23)
  * debugger statement left in code (P23)
  * duplicate arguments in function definitions (P23)
  * duplicate case label in switch statements (P23)
  * empty block statements (P23)
  * extraneous semicolons (P23)
  * reassigning function declarations (P23)
  * assignment to native objects or read-only global variables (P23)
  * variable redeclaration (P23)

* CSS programming standards 
  * empty rules (P22)
  * CSS cannot be parsed (P23)
  * unknown properties or property values (P23)
  * duplicate properties (P23)

Suggested improvements may be made regarding your code quality based on programming standards not explicitly listed above. They will not result in any deductions for this assignment but you are encouraged to incorporate the suggestions in your future work.